<?php
declare(strict_types=1);

// Chargement autoloader de composer.
require_once(__DIR__.'/vendor/autoload.php');

// Définition du fuseau horaire.
date_default_timezone_set('Europe/Paris');

//! ----------------

$animaux = [
    'chiens' => [
        'Malus' => 'appartenait a papa',
        'Reglisse' => 'appartenait a maman',
        'Happy' => 'appartenait a papa & Zab'
    ],
    'chats' => [
        'Zebulon' => 'appartenait a maman',
        'Leon' => 'appartenait a maman',
        'Fructose' => 'appartenait a Bat',
        'Necko' => 'appartient a maman',
        'Charlie' => 'appartient a maman'
    ]
];

returnJsonHttpResponse($animaux, 200);
