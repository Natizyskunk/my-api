## my-api  

Create your own api server via PHP or Javascript.

### How-To

#### PHP
1. Clone project and `cd` in it.
2. Open a terminal and type `composer install -o`.
3. Open index.php with webserver or via `php -S 0.0.0:8000`.
4. Open web browser to http://localhost:8000/.

#### Javascript
1. Clone project and `cd` in it.
2. Open a terminal and type `npm install`.
3. In terminal type `npm run start`.
4. Open web browser to http://localhost:8000/.
