"use strict";

// Chargement des modules js.
import { createServer } from 'http';
import { returnJsonHttpResponse } from './functions/returnJsonHttpResponse.js';

// Définition du fuseau horaire.
process.env.TZ = 'Europe/Paris';
new Date().toLocaleString('fr-FR', {timeZone: 'Europe/Paris'});

//! ----------------

let animaux = {
    'chiens' : {
        'Malus' : 'appartenait a papa',
        'Reglisse' : 'appartenait a maman',
        'Happy' : 'appartenait a papa & Zab'
    },
    'chats' : {
        'Zebulon' : 'appartenait a maman',
        'Leon' : 'appartenait a maman',
        'Fructose' : 'appartenait a Bat',
        'Necko' : 'appartient a maman',
        'Charlie' : 'appartient a maman'
    }
};

createServer((req, res) => {
    returnJsonHttpResponse(animaux, 200, res);
}).listen(8000, 'localhost');
