/**
 * Return Formatted json with correct headers.
 * 
 * @param Array $data 
 * @param Int $responseCode
 * @param Object $serverResponse
 * @return Array
 */
 export function returnJsonHttpResponse(data, responseCode, serverResponse) {
    serverResponse.setHeader('Access-Control-Allow-Origin', '*');
    serverResponse.setHeader('Content-Type', 'application/json; charset=utf-8');
    serverResponse.writeHead(responseCode, {'Content-Type': 'application/json; charset=utf-8'});
    
    const responseStatus = {statusCode: responseCode};

    // Setting HTTP response code.  
    switch (responseCode) {
        case 200:
            console.log([responseStatus, 'Success']);
            serverResponse.end(JSON.stringify(data));
            break;
        case 201:
            console.log([responseStatus, 'Created']);
            break;
        case 202:
            console.log([responseStatus, 'Accepted']);
            break;
        case 204:
            console.log([responseStatus, 'No Content']);
            break;
        case 301:
            console.log([responseStatus, 'Moved Permanently']);
            break;
        case 302:
            console.log([responseStatus, 'Found']);
            break;
        case 400:
            console.log([responseStatus, 'Bad Request']);
            break;
        case 401:
            console.log([responseStatus, 'Unauthorized']);
            break;
        case 403:
            console.log([responseStatus, 'Forbidden']);
            break;
        case 404:
            console.log([responseStatus, 'Not Found']);
            break;
        case 405:
            console.log([responseStatus, 'Not Acceptable']);
            break;
        case 408:
            console.log([responseStatus, 'Requests Timeout']);
            break;
        case 429:
            console.log([responseStatus, 'Too Many Requests']);
            break;
        case 500:
            console.log([responseStatus, 'Internal Server Error']);
            break;
        case 501:
            console.log([responseStatus, 'Not Implemented']);
            break;
        case 502:
            console.log([responseStatus, 'Bad Gateway']);
            break;
        case 503:
            console.log([responseStatus, 'Service Unavailable']);
            break;
        default:
            break;
    }

    // Making sure nothing is added.
    return;
}
