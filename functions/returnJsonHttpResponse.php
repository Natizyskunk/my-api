<?php
/**
 * Return Formatted json with correct headers.
 * 
 * @param Array $data 
 * @param Int $responseCode
 * @return Array
 */
function returnJsonHttpResponse($data, int $responseCode):array {
    // Removing any string that could create an invalid JSON such as PHP Notice, Warning, logs...
    ob_get_clean();
    
    // Cleaning any previously added headers, to start fresh.
    header_remove(); 

    // Adding necessary headers.
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json; charset=utf-8');
    http_response_code($responseCode);

    define('__statusCode__', $responseCode);

    // Setting HTTP response code.
    switch ($responseCode) {
        case 200:
            // echo "[{statusCode: ".__statusCode__."}, 'Success']";
            echo json_encode($data, JSON_PRETTY_PRINT);
            break;
        case 201:
            echo "[{statusCode: ".__statusCode__."}, 'Created']";
            break;
        case 202:
            echo "[{statusCode: ".__statusCode__."}, 'Accepted']";
            break;
        case 204:
            echo "[{statusCode: ".__statusCode__."}, 'No Content']";
            break;
        case 301:
            echo "[{statusCode: ".__statusCode__."}, 'Moved Permanently']";
            break;
        case 302:
            echo "[{statusCode: ".__statusCode__."}, 'Found']";
            break;
        case 400:
            echo "[{statusCode: ".__statusCode__."}, 'Bad Request']";
            break;
        case 401:
            echo "[{statusCode: ".__statusCode__."}, 'Unauthorized']";
            break;
        case 403:
            echo "[{statusCode: ".__statusCode__."}, 'Forbidden']";
            break;
        case 404:
            echo "[{statusCode: ".__statusCode__."}, 'Not Found']";
            break;
        case 405:
            echo "[{statusCode: ".__statusCode__."}, 'Not Acceptable']";
            break;
        case 408:
            echo "[{statusCode: ".__statusCode__."}, 'Requets Timeout']";
            break;
        case 429:
            echo "[{statusCode: ".__statusCode__."}, 'Too Many Requests']";
            break;
        case 500:
            echo "[{statusCode: ".__statusCode__."}, 'Internal Server Error']";
            break;
        case 501:
            echo "[{statusCode: ".__statusCode__."}, 'Not Implemented']";
            break;
        case 502:
            echo "[{statusCode: ".__statusCode__."}, 'Bad Gateway']";
            break;
        case 503:
            echo "[{statusCode: ".__statusCode__."}, 'Service Unavailable']";
            break;
        default:
            break;
    }
    
    // Making sure nothing is added.
    exit();
}
